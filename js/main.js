$(function() {


    var $body = $(document.body),
        $html = $(document.documentElement),
        formOpened = 'opened',
        overflowHidden = 'oveflowHidden';

    function formPopup($btn, $wrap) {

        var closeForm = $('.formExtraWrapper .close_btn'),
            formWrap = $($wrap),
            formBtn = $($btn);

        closeForm.on('click', function() {
            formWrap.removeClass(formOpened);
            $html.removeClass(overflowHidden);
            $('.role_block').removeClass('active');
        });

        $(document).on('click', $btn, function(event) {
            if ($btn == '.add_role_item' || $btn == '.role_edit') {
                $(this).closest('.role_block').addClass('active');
            }

            if ($btn == '.role_edit') {
                const nameRole = $(this).parents('.role_block').find('.role_name').text();
                console.log(nameRole);
                formWrap.find('input').val(nameRole);
            }

            formWrap.addClass(formOpened);
            $html.toggleClass(overflowHidden);
            event.preventDefault();
        });

        $html.on('keyup', function(event) {
            if (formWrap.hasClass(formOpened) && event.keyCode == 27) {
                formWrap.removeClass(formOpened);
                $html.removeClass(overflowHidden);
                $('.role_block').removeClass('active');
            }
        });
        $body.on('click touchstart', function(a) {
            if ($(a.target).closest('.formExtraWrapper').length || $(a.target).closest(formBtn).length) return;
            if (formWrap.hasClass(formOpened)) {
                formWrap.removeClass(formOpened);
                $html.removeClass(overflowHidden);
                $('.role_block').removeClass('active');
            }
        });
    }

    formPopup('.add_role', '.add_role_popup');
    formPopup('.role_edit', '.add_role_popup');
    formPopup('.add_role_item', '.add_store_popup');

    $(document).on('click', '.role_actions', function() {
        $(this).addClass('open');
        $(this).parent('.role_block').find('.role_actions_btns').slideDown();
    });

    $body.on('click touchstart', function(a) {
        if ($(a.target).closest('.role_actions').length) return;
        if ($('.role_actions').hasClass('open')) {
            $('.role_actions').removeClass('open');
            $('.role_actions_btns').slideUp();
        }
    });

    $('.add_role_popup .btn_save').on('click', function() {
        const inputValue = $(this).closest('.form_content').find('input').val();

        if ($('.role_block').hasClass('active')) {
            $(document).find('.role_block.active .role_name').text(inputValue);
        } else {

            if (inputValue != '') {
                const roleItem = `<div class="role_block">
                            <div class="role_actions"></div>
                            <div class="role_actions_btns">
                                <div class="btn_button role_edit"><span>Edit</span></div>
                                <div class="btn_button role_delete"><span>Delete</span></div>
                            </div>
                            <div class="role_name">${inputValue}</div>
                            <div class="role_text">I want to be able to:</div>
                            <div class="role_inner">
                            </div>
                            <div class="btn_button btn_outline btn_icon_left_add btn_ls add_role_item">Add</div>
                        </div>`;

                $('.role_block_wrap').prepend(roleItem);
            }
        }

        $('.add_role_popup').removeClass(formOpened);
        $html.removeClass(overflowHidden);
        $(this).closest('.form_content').find('input').val('');
        $('.role_block').removeClass('active');
    });

    $('.add_store_popup .btn_save').on('click', function() {
        const inputValue = $(this).closest('.form_content').find('input').val();
        if (inputValue != '') {
            const roleItem = `<div class="role_item">
                    <span>${inputValue}</span>
                    <div class="role_item_delete"></div>
                </div>`;

            $('.role_block.active .role_inner').append(roleItem);

            $('.add_store_popup').removeClass(formOpened);
            $html.removeClass(overflowHidden);
            $(this).closest('.form_content').find('input').val('');
            $('.role_block').removeClass('active');
        }
    });

    $(document).on('click', '.role_delete', function() {
        $(this).closest('.role_block').remove();
    });

    $(document).on('click', '.role_item_delete', function() {
        $(this).closest('.role_item').remove();
    });

    var currentTab = 0;

    showTab(currentTab);

    $('#prevBtn').on('click', function() {
        nextPrev(-1);
    });

    $('input[type="checkbox"]').on('change', function(event) {

        if (!$(this).closest('.field.required').hasClass('no_checked') && !$(this).is(":checked")) {
            $(this).closest('.field.required').addClass('no_checked');
        } else {
            $(this).closest('.field.required').removeClass('no_checked').removeClass('error');
        }
    })

    $('#nextBtn').on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('btn_send_form')) {
            let flag = false;
            let input = $(document).find('.saas_step.active_step .field.required input[type="text"]');
            let valueInput = input.val();

            let inputField = $(document).find('.saas_step.active_step .field.required');

            inputField.each(function() {
                var valueInput = $(this).find('input').val();

                if ($(this).hasClass('required') && (valueInput == '' || $(this).hasClass('no_checked'))) {
                    $(this).addClass('error');
                    if (!$(this).find('.error_text').length) {
                        $(this).append('<div class="error_text">Error text</div>');
                    };
                } else {
                    $(this).removeClass('error');
                };
            });



            if (!$(this).closest('form').find('.saas_step.form_step .field').hasClass('no_checked') && !$(this).closest('form').find('.saas_step.form_step .field').hasClass('error')) {
                $(this).closest('.saas_right_content_btns').hide();
                nextPrev(1);

            }
        } else {
            nextPrev(1);
        }
    });

    function showTab(n) {
        let step = $('.saas_step');

        step.each(function(e) {
            if (currentTab == e) {
                $(this).addClass('active_step').removeClass('hide');
            }
        })

        if (n == 0) {
            $("#prevBtn").css('display', 'none');
        } else {
            $("#prevBtn").css('display', 'inline-flex');
        }

        if (n == (step.length - 2)) {
            $("#nextBtn").addClass('btn_send_form');
        } else {
            if ($("#nextBtn").hasClass('btn_send_form')) {
                $("#nextBtn").removeClass('btn_send_form');
            }
        }

        fixStepIndicator(n);
    }

    function nextPrev(n) {
        let step = $('.saas_step');
        if (n == 1 && !validateForm()) return false;
        step.each(function() {
            $(this).addClass('hide').removeClass('active_step');
        })

        currentTab = currentTab + n;


        showTab(currentTab);
    }

    function fixStepIndicator(n) {
        var i, x = document.getElementsByClassName("saas_left_steps_item");

        for (i = 0; i < x.length; i++) {
            x[i].className = x[i].className.replace(" active", "");
        }
        x[n].className += " active";
    }

    function validateForm() {
        var x, i, valid = true;
        x = document.getElementsByClassName("saas_step");

        if (valid) {
            $(".saas_left_steps_item").each(function(e) {
                if (currentTab == e && !$(this).hasClass('done')) {
                    $(this).addClass('done');
                }
            })
        }
        return valid;
    }

    $('.btn_recalculate').on('click', function() {
        location.reload();
        return false;
    });


    var regx = new RegExp(
        "^" +
        // protocol identifier
        "(?:(?:https?|http)://)" +
        // user:pass authentication
        "(?:\\S+(?::\\S*)?@)?" +
        "(?:" +
        // IP address exclusion
        // private & local networks
        "(?!(?:10|127)(?:\\.\\d{1,3}){3})" +
        "(?!(?:169\\.254|192\\.168)(?:\\.\\d{1,3}){2})" +
        "(?!172\\.(?:1[6-9]|2\\d|3[0-1])(?:\\.\\d{1,3}){2})" +
        // IP address dotted notation octets
        // excludes loopback network 0.0.0.0
        // excludes reserved space >= 224.0.0.0
        // excludes network & broacast addresses
        // (first & last IP address of each class)
        "(?:[1-9]\\d?|1\\d\\d|2[01]\\d|22[0-3])" +
        "(?:\\.(?:1?\\d{1,2}|2[0-4]\\d|25[0-5])){2}" +
        "(?:\\.(?:[1-9]\\d?|1\\d\\d|2[0-4]\\d|25[0-4]))" +
        "|" +
        // host name
        "(?:(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)" +
        // domain name
        "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*" +
        // TLD identifier
        "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))" +
        ")" +
        // port number
        "(?::\\d{2,5})?" +
        // resource path
        "(?:/\\S*)?" +
        "$", "i");

    $('input[type="url"]').on("input", function(e) {
        var str = $(this).val();
        var isURL = regx.test(str);
        if (isURL == true) {
            $(this).parent().removeClass('error');            
        } else {
            $(this).parent().addClass('error');
            if (!$(this).parent().find('.error_text').length) {
                $(this).parent().append('<div class="error_text">Error url</div>');
            }
        }
    });
});